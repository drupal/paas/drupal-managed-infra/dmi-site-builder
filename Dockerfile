ARG BASE_VERSION

FROM gitlab-registry.cern.ch/drupal/paas/drupal-managed-infra/dmi-base:${BASE_VERSION}

LABEL io.k8s.description="Drupal managed infra site-builder" \
      io.k8s.display-name="Drupal 8 Managed Infra site-builder" \
      io.openshift.tags="managed,drupal,site-builder" \
      maintainer="Drupal Admins <drupal-admins@cern.ch>"

ADD fix-permissions /fix-permissions
RUN chmod +x /fix-permissions

# DRUPAL specific. Include drupal tools in the PATH
# DRUPAL_APP_DIR is the drupal final destination. Must be in consonance with nginx.
# DRUPAL_CODE_DIR is used to pre-set drupal code.
ENV DRUPAL_APP_DIR /app
ENV DRUPAL_CODE_DIR /code
ENV HOME ${DRUPAL_CODE_DIR}
RUN mkdir -p ${DRUPAL_CODE_DIR} ${DRUPAL_APP_DIR}

# From here we will manage all central/non central modules + drupal version
RUN git clone https://gitlab.cern.ch/drupal/paas/composer-drupal-project ${DRUPAL_CODE_DIR}    

WORKDIR ${DRUPAL_CODE_DIR}
# Composer as root/super user! See https://getcomposer.org/root for details
# Set up drupal minimum stack
RUN composer install --optimize-autoloader --no-cache -vv	

# TO BE REMOVED
# Web Team modules and themes. This needs to be removed in favour of composer
ADD toremove.sh /
RUN chmod +x /toremove.sh && /toremove.sh
# TO BE REMOVED

# Add extra configurations
# At this point, composer has created the required settings.php through:
# post-update-cmd: DrupalProject\composer\ScriptHandler::createRequiredFiles
# Overwrite settings.php with ours.
# - settings.php
ADD config/sites/default/settings.php ${DRUPAL_CODE_DIR}/web/sites/default/settings.php
# Remove ${DRUPAL_CODE_DIR}/web/sites/default/files, preparing it to be symbolic link after init-app.sh;
RUN rm -rf ${DRUPAL_CODE_DIR}/web/sites/default/files
# Fix-permissions
RUN /fix-permissions ${DRUPAL_CODE_DIR}

# Add init-container for every container that contains drupal code
ADD init-app.sh /
RUN chmod +x /init-app.sh